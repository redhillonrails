require File.dirname(__FILE__) + '/../test_helper'
require 'countries_controller'

class CountriesController; def rescue_action(e) raise e end; end

class CountriesControllerTest < Test::Unit::TestCase
  def setup
    @controller = CountriesController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_cascading_stylesheets
    get :index
    assert_response :success
    assert_template 'index'

    assert_select 'html' do
      assert_select 'head' do
        assert_cascade('application', 'countries', 'countries/index') { |name| stylesheet_link_selector(name) }
      end
    end
  end

  def test_cascading_javascripts
    get :index
    assert_response :success
    assert_template 'index'

    assert_select 'html' do
      assert_select 'head' do
        assert_cascade('countries/index') { |name| javascript_include_selector(name) }
      end
    end
  end

  private

  def assert_cascade(*names, &block)
    names.each { |name| assert_select((yield name), 1) }
    assert_select names.map { |name| (yield name) }.join(' + ')
  end

  def stylesheet_link_selector(name)
    "link[rel=stylesheet][href^=/stylesheets/#{name}.css?]"
  end

  def javascript_include_selector(name)
    "script[type=text/javascript][src^=/javascripts/#{name}.js?]"
  end
end
