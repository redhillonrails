require File.dirname(__FILE__) + '/../test_helper'

class OrderLinesTest < Test::Unit::TestCase
  def test_validates_presence_of_blank_quantity
    line = OrderLine.new(:quantity => '')
    assert !line.valid?
    assert_equal "can't be blank", line.errors[:quantity]
  end
end
