require File.dirname(__FILE__) + '/../test_helper'

class StateTest < Test::Unit::TestCase
	def test_is_invalid_when_blank
    state = State.new
    assert !state.valid?
    errors = state.errors
    
    assert_equal "can't be blank", errors[:name]
    assert_equal "can't be blank", errors[:abbreviation]
    assert_equal "can't be blank", errors[:country_id]

    assert_equal 3, errors.size
	end
	
	def test_is_valid_when_fully_populated
    state = State.new(:name => 'Tasmania', :abbreviation => 'TAS', :country => countries(:australia))
    assert state.valid?
    assert state.errors.empty?
  end
  
  def test_belongs_to_country
    assert_equal countries(:usa), states(:california).country
  end
  
  def test_has_many_cities
    cities = states(:victoria).cities
    assert_not_nil cities
    assert cities.include?(cities(:melbourne))
    assert cities.include?(cities(:west_melbourne))
    assert_equal 2, cities.size
  end
  
  def test_simultaneously_create_new_state_and_country
    country = Country.new(:name => 'Japan', :isocode => 'JP', :iddcode => '81')
    country.states.build(:name => 'Ibaraki', :abbreviation => 'IBA', :country => country)
    country.save!
  end

  def test_is_invalid_when_required_association_is_cleared
    state = states(:california)
    state.country = nil
    state.valid?
    errors = state.errors
    
    assert_equal "can't be blank", errors[:country_id]
    assert_equal 1, errors.size
  end
  
  def test_is_invalid_when_required_foreign_key_is_cleared
    state = states(:california)
    state.country_id = nil
    state.valid?
    errors = state.errors
    
    assert_equal "can't be blank", errors[:country_id]
    assert_equal 1, errors.size
  end
  
  def test_cannot_create_when_name_is_already_used_within_a_country
    state = State.new(:name => 'Victoria', :abbreviation => 'xxx', :country => countries(:australia))
    assert !state.valid?
    errors = state.errors

    assert_equal "has already been taken", errors[:name]
    assert_equal 1, errors.size
  end
  
  def test_cannot_create_when_abbreviation_is_already_used_within_a_country
    state = State.new(:name => 'xxx', :abbreviation => 'VIC', :country => countries(:australia))
    assert !state.valid?
    errors = state.errors

    assert_equal "has already been taken", errors[:abbreviation]
    assert_equal 1, errors.size
  end
end
