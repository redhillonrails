require File.dirname(__FILE__) + '/../test_helper'

class SystemSettingTest < Test::Unit::TestCase
  def test_indexed_find_with_nil
    assert_nil SystemSetting[nil]
  end
  
  def test_find_by_name_with_nil
    assert_nil SystemSetting.find_by_name(nil)
  end
  
  def test_indexed_find_with_string
    assert_equal system_settings(:environment), SystemSetting["environment"]
  end
  
  def test_indexed_find_with_symbol
    assert_equal system_settings(:environment), SystemSetting[:environment]
  end
  
  def test_find_by_name_with_string
    assert_equal system_settings(:environment), SystemSetting.find_by_name("environment")
  end

  def test_find_by_name_with_symbol
    assert_equal system_settings(:environment), SystemSetting.find_by_name(:environment)
  end
end
