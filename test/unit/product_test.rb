require File.dirname(__FILE__) + '/../test_helper'

class ProductTest < Test::Unit::TestCase
	def test_product_is_base_class
	  assert Product.base_class?
  end

  def test_manufactured_product_is_not_base_class
    assert !ManufacturedProduct.base_class?
  end

  def test_assembled_product_is_not_base_class
    assert !AssembledProduct.base_class?
  end

  def test_has_many_orders_through_order_lines
    assert_equal 2, products(:lamp).orders.size
  end

  def test_type_is_populated_before_validation
    product = AssembledProduct.new
    product.valid?
    errors = product.errors
    assert !errors.include?(:type)
  end

  def test_categories_habtm_association
    product = products(:lamp)
    assert_equal 3, product.categories.size
    assert product.categories.include?(product_categories(:home))
    assert product.categories.include?(product_categories(:office))
    assert product.categories.include?(product_categories(:lighting))
  end

  def test_manual_associations_are_not_overwritted
    product = products(:lamp)
    product.order_lines.largest
  end
end
