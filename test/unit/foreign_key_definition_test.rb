require File.dirname(__FILE__) + '/../test_helper'

class ForeignKeyDefinitionTest < Test::Unit::TestCase
	def test_base
	  definition = RedHillConsulting::Core::ActiveRecord::ConnectionAdapters::ForeignKeyDefinition.new(nil, 'orders', 'company_id', 'companies', 'id', nil, nil, false)
	  assert_equal "FOREIGN KEY (company_id) REFERENCES companies (id)", definition.to_sql
	  assert_equal "add_foreign_key \"orders\", [\"company_id\"], \"companies\", [\"id\"]", definition.to_dump
	end

	def test_name
	  definition = RedHillConsulting::Core::ActiveRecord::ConnectionAdapters::ForeignKeyDefinition.new('a_foreign_key', 'orders', 'company_id', 'companies', 'id', nil, nil, false)
	  assert_equal "CONSTRAINT a_foreign_key FOREIGN KEY (company_id) REFERENCES companies (id)", definition.to_sql
	  assert_equal "add_foreign_key \"orders\", [\"company_id\"], \"companies\", [\"id\"], :name => \"a_foreign_key\"", definition.to_dump
	end

	def test_on_update_with_on_delete
	  definition = RedHillConsulting::Core::ActiveRecord::ConnectionAdapters::ForeignKeyDefinition.new(nil, 'orders', 'company_id', 'companies', 'id', :restrict, :cascade)
	  assert_equal "FOREIGN KEY (company_id) REFERENCES companies (id) ON UPDATE RESTRICT ON DELETE CASCADE", definition.to_sql
	  assert_equal "add_foreign_key \"orders\", [\"company_id\"], \"companies\", [\"id\"], :on_update => :restrict, :on_delete => :cascade", definition.to_dump
	end

	def test_deferrable
	  definition = RedHillConsulting::Core::ActiveRecord::ConnectionAdapters::ForeignKeyDefinition.new(nil, 'orders', 'company_id', 'companies', 'id', nil, nil, true)
	  assert_equal "FOREIGN KEY (company_id) REFERENCES companies (id) DEFERRABLE", definition.to_sql
	  assert_equal "add_foreign_key \"orders\", [\"company_id\"], \"companies\", [\"id\"], :deferrable => true", definition.to_dump
	end

	def test_on_delete_with_deferrable
	  definition = RedHillConsulting::Core::ActiveRecord::ConnectionAdapters::ForeignKeyDefinition.new(nil, 'orders', 'company_id', 'companies', 'id', nil, :cascade, true)
	  assert_equal "FOREIGN KEY (company_id) REFERENCES companies (id) ON DELETE CASCADE DEFERRABLE", definition.to_sql
	  assert_equal "add_foreign_key \"orders\", [\"company_id\"], \"companies\", [\"id\"], :on_delete => :cascade, :deferrable => true", definition.to_dump
	end
end
