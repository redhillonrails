require File.dirname(__FILE__) + '/../test_helper'

class CustomerTest < Test::Unit::TestCase
	def test_is_invalid_when_blank
    customer = Customer.new
    assert !customer.valid?
    errors = customer.errors
    assert_equal 3, errors.size
    assert_equal "can't be blank", errors[:name]
    assert_equal "can't be blank", errors[:street]
    assert_equal "can't be blank", errors[:city_id]
	end

	def test_is_valid_when_fully_populated
    customer = Customer.new(:name => 'ThoughtWorks, Inc.', :street => 'Level 11, 155 Queen Street', :city => cities(:melbourne))
    assert customer.valid?
    assert customer.errors.empty?
  end

  def test_has_many_orders
    orders = customers(:redhill).orders

    assert orders.include?(orders(:redhill_lamps))
    assert orders.include?(orders(:redhill_pens))

    assert_equal 2, orders.size
  end

  def test_status_is_required_on_update_even_though_it_has_a_default
    customer = customers(:redhill)
    customer.status = nil
    assert !customer.valid?
    errors = customer.errors
    assert_equal 1, errors.size
    assert_equal "can't be blank", errors[:status]
  end
  
  def test_has_parent
    assert_equal customers(:redhill), customers(:redhill_usa).parent
  end
  
  def test_invalid_fax_format
    customer = Customer.new
    customer.fax = "abc"
    assert !customer.valid?
    assert_equal "is invalid", customer.errors[:fax]
  end
  
  def test_invalid_phone_format
    customer = Customer.new
    customer.phone = "abc"
    assert !customer.valid?
    assert_equal "is invalid", customer.errors[:phone]
  end

  def test_blanks_are_stripped_from_name_using_setter
    customer = Customer.new
    customer.name = "    this is a name    "
    assert_equal "this is a name", customer.name
  end
  
  def test_blanks_are_stripped_from_name_using_indexed_setter
    customer = Customer.new
    customer["name"] = "    this is a name    "
    assert_equal "this is a name", customer.name
  end

  def test_blanks_are_stripped_from_name_using_write_attribute
    customer = Customer.new
    customer.write_attribute("name", "    this is a name    ")
    assert_equal "this is a name", customer.name
  end

  def test_blanks_are_stripped_from_name_using_bulk_setter
    customer = Customer.new
    customer.attributes = { :name => "    this is a name    " }
    assert_equal "this is a name", customer.name
  end
  
  def test_normal_customers
    customers = Customer.normal
    assert_not_nil customers
    assert_equal 2, customers.size
  end
end
