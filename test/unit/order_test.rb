require File.dirname(__FILE__) + '/../test_helper'

class OrderTest < Test::Unit::TestCase
  def test_belongs_to_customer
    assert_equal customers(:apple), orders(:apple_full_hamburger).ordered_by
  end

  def test_has_one_invoice
    assert_equal invoices(:redhill_pens), orders(:redhill_pens).invoice
  end

  def test_has_many_lines
    lines = orders(:apple_full_hamburger).lines

    assert lines.include?(order_lines(:apple_full_hamburger_desks))
    assert lines.include?(order_lines(:apple_full_hamburger_lamps))
    assert lines.include?(order_lines(:apple_full_hamburger_pens))
    assert lines.include?(order_lines(:apple_full_hamburger_pencils))

    assert_equal 4, lines.size
  end

  def test_lines_sorted_by_position
    last_position = 0
    orders(:apple_full_hamburger).lines.each do |line|
      assert line.position > last_position, "#{line.position} > #{last_position}"
      last_position = line.position
    end
  end

  def test_customer_reference_is_unique_within_customer_id
    order = Order.new(:ordered_by => customers(:redhill), :customer_reference => "SUPPL001")
    assert !order.valid?
    assert_equal "has already been taken", order.errors[:customer_reference]
  end

  def test_nil_customer_reference_is_unique_within_customer_id
    order = Order.new(:ordered_by => customers(:apple))
    order.valid?
    assert_nil order.errors[:customer_reference]
  end

  def test_has_many_products_through_order_lines
    orders(:apple_full_hamburger).products.size == 4
  end

  def test_deleting_an_order_removes_order_lines
    orders(:redhill_pens).destroy
  end
end
