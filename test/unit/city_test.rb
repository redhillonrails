require File.dirname(__FILE__) + '/../test_helper'

class CityTest < Test::Unit::TestCase
	def test_is_invalid_when_blank
    city = City.new
    assert !city.valid?
    errors = city.errors
    assert_equal 3, errors.size
    assert_equal "can't be blank", errors[:name]
    assert_equal "can't be blank", errors[:postcode]
    assert_equal "can't be blank", errors[:state_id]
	end

	def test_is_valid_when_fully_populated
    city = City.new(:name => 'Elsternwick', :postcode => '3185', :state => states(:victoria))
    assert city.valid?
    assert city.errors.empty?
  end

  def test_belongs_to_state
    assert_equal states(:california), cities(:cupertino).state
  end

  def test_name_containing_only_spaces_is_invalid
    city = City.new(:name => "                           ")
    assert !city.valid?
    errors = city.errors
    assert_equal "can't be blank", errors[:name]
  end
end
