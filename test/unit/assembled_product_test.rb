require File.dirname(__FILE__) + '/../test_helper'

class ChildOfAssembledProduct < AssembledProduct
end

class AssembledProductTest < Test::Unit::TestCase
  def test_sti_validations
    # make sure AssembledProduct has validations
    x = products(:lamp)
    assert x.kind_of?(AssembledProduct)
    assert_valid x
    x.name = nil
    x.valid?
    errors = x.errors
    assert_equal 1, errors.size

    # make sure ChildOfAssembledProduct also has
    # validations, which it should not have inherited
    # from it's parent as at the time of creating
    # the ChildOfAssembledProduct class it's parent
    # did not have it's schema validations loaded
    y = ChildOfAssembledProduct.new
    assert !y.valid?
    assert_equal 2, y.errors.size
  end

  def test_sti_associations
    # make sure AssembledProduct has associations
    x = products(:lamp)
    assert x.kind_of?(AssembledProduct)
    assert x.respond_to?(:order_lines)

    # make sure ChildOfAssembledProduct also has
    # assoictaions, which it should not have inherited
    # from it's parent as at the time of creating
    # the ChildOfAssembledProduct class it's parent
    # did not have it's foreign key associations loaded
    y = ChildOfAssembledProduct.new
    assert y.respond_to?(:order_lines)
  end
end
