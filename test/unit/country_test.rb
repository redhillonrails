require File.dirname(__FILE__) + '/../test_helper'

class CountryTest < Test::Unit::TestCase
	def test_is_invalid_when_blank
    country = Country.new
    assert !country.valid?
    errors = country.errors
    assert_equal 3, errors.size
    assert_equal "can't be blank", errors[:name]
    assert_equal "can't be blank", errors[:isocode]
    assert_equal "can't be blank", errors[:iddcode]
	end

	def test_is_valid_when_fully_populated
    country = Country.new(:name => 'United Kingdom', :isocode => 'UK', :iddcode => '44')
    assert country.valid?
    assert country.errors.empty?
  end

  def test_has_many_states
    states = countries(:australia).states
    assert_not_nil states
    assert_equal 3, states.size
    assert states.include?(states(:victoria))
    assert states.include?(states(:nsw))
    assert states.include?(states(:wa))
  end

  def test_has_many_cities_through_states
    countries(:australia).cities.size == 4
  end

  def test_name_is_case_insensitive
    country = Country.new(:name => countries(:australia).name.upcase)

    assert !country.valid?
    errors = country.errors
    assert_equal "has already been taken", errors[:name]
  end
end
