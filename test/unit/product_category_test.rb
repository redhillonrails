require File.dirname(__FILE__) + '/../test_helper'

class ProductCategoryTest < Test::Unit::TestCase
  def test_products_habtm_association
    product_category = product_categories(:office)
    assert_equal 4, product_category.products.size
    assert product_category.products.include?(products(:desk))
    assert product_category.products.include?(products(:lamp))
    assert product_category.products.include?(products(:pen))
    assert product_category.products.include?(products(:pencil))
  end
end
