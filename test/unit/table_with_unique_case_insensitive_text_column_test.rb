require File.dirname(__FILE__) + '/../test_helper'

class TableWithUniqueCaseInsensitiveTextColumnTest < Test::Unit::TestCase
  def test_it
    TableWithUniqueCaseInsensitiveTextColumn.create(:a_column => "avalue")
    model = TableWithUniqueCaseInsensitiveTextColumn.new(:a_column => "AVALUE")
    assert !model.valid?
    errors = model.errors
    assert_equal "has already been taken", errors[:a_column]
  end
end
