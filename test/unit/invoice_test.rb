require File.dirname(__FILE__) + '/../test_helper'

class InvoiceTest < Test::Unit::TestCase
  def test_validates_uniqueness_of_order
    invoice = Invoice.new(:order => orders(:redhill_pens))
    assert !invoice.valid?
    errors = invoice.errors

    assert_equal 1, errors.size
    assert_equal "has already been taken", errors[:order_id]
  end

  def test_belongs_to_order
    assert_equal orders(:redhill_pens), invoices(:redhill_pens).order
  end
end
