require File.dirname(__FILE__) + '/../test_helper'

class PostgresqlAdapterTest < Test::Unit::TestCase
  def test_deferrable
    adapter = Adapter.new do |sql, name|
      assert_equal(<<-SQL, sql)
        SELECT f.conname, pg_get_constraintdef(f.oid), t.relname
          FROM pg_class t, pg_constraint f
         WHERE f.conrelid = t.oid
           AND f.contype = 'f'
           AND t.relname = 'customers'
      SQL
      assert_equal "test_deferrable", name
      [[nil, "FOREIGN KEY (country_id) REFERENCES countries(id) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE"]]
    end

    foreign_keys = adapter.foreign_keys("customers", "test_deferrable")
    assert_equal 1, foreign_keys.size
    foreign_key = foreign_keys.first
    assert_not_nil foreign_key
    assert_nil foreign_key.table_name
    assert_equal ["country_id"], foreign_key.column_names
    assert_equal "countries", foreign_key.references_table_name
    assert_equal ["id"], foreign_key.references_column_names
    assert_equal :cascade, foreign_key.on_update
    assert_equal :restrict, foreign_key.on_delete
    assert foreign_key.deferrable
  end

  class Adapter
    def initialize(&block)
      @block = block
    end

    def query(sql, name)
      @block.call(sql, name)
    end

    def indexes(table, name)
    end

    include RedHillConsulting::Core::ActiveRecord::ConnectionAdapters::PostgresqlAdapter
  end
end
