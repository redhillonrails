module RedHillConsulting::CascadingJavascripts::ActionView::Helpers
  module AssetTagHelper
    def self.included(base)
      base.class_eval do
        alias_method_chain :expand_javascript_sources, :cascade
      end
    end

    def expand_javascript_sources_with_cascade(sources)
      cascade = sources.delete(:cascades)
      expanded = expand_javascript_sources_without_cascade(sources)
      if cascade
        [@controller.controller_name, "#{@controller.controller_name}/#{@controller.action_name}"].each do |source|
          expanded << source if File.exists?(File.join(ActionView::Helpers::AssetTagHelper::JAVASCRIPTS_DIR, source + '.js'))
        end
      end
      expanded
    end
  end
end
