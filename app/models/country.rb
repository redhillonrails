class Country < AbstractModel
  has_many :cities, :through => :states
end
