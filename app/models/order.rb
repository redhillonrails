class Order < AbstractModel
  has_many :products, :through => :lines
end
