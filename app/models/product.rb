class Product < AbstractModel
  has_many :order_lines do
    def largest
      find(:all, :order => ["quantity DESC"], :limit => 5)
    end
  end

  has_many :orders, :through => :order_lines
end
