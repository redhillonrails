class Customer < AbstractModel
  schema_validations :except => [:code], :on => :create
  
  def self.normal
    find :all, :from => 'normal_customers'
  end

  def before_create
    self.code = "#{city.country.isocode[0..1]}#{name[0..3]}".upcase if code.nil?
  end
end
