class AbstractModel < ActiveRecord::Base
  class << self
    def validates_phone(*attr_names)
      validates_format_of attr_names, :with => /[0-9]+(\.[0-9]+)*/, :allow_nil => true 
    end
    alias :validates_fax :validates_phone
  end
end
