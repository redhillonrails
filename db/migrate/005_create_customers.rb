class CreateCustomers < ActiveRecord::Migration
  def self.up
    create_table :customers do |t|
      t.column :name,     :string,  :null => false, :limit => 30
      t.column :street,   :string,  :null => false, :limit => 30
      t.column :city_id,  :integer, :null => false
      t.column :code,     :string,  :null => false, :limit => 6
      t.column :status,   :string,  :null => false, :limit => 10, :default => 'normal'
      t.column :phone,    :string,  :null => true,  :limit => 15
      t.column :fax,      :string,  :null => true,  :limit => 15
    end

    add_index :customers, [:name], :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :customers
  end
end
