class CreateOrderLines < ActiveRecord::Migration
  def self.up
    create_table :order_lines do |t|
      t.column :order_id,   :integer, :null => false, :on_delete => :cascade, :on_update => :cascade
      t.column :position,   :integer, :null => false
      t.column :quantity,   :integer, :null => false
    end

    add_column :order_lines, :product_id, :integer, :null => false

    add_index :order_lines, [:order_id, :position], :unique => true    
  end

  def self.down
    drop_table :order_lines
  end
end
