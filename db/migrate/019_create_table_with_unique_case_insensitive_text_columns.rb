class CreateTableWithUniqueCaseInsensitiveTextColumns < ActiveRecord::Migration
  def self.up
    create_table :table_with_unique_case_insensitive_text_columns do |table|
      table.text :a_column, :null => false
    end
    add_index :table_with_unique_case_insensitive_text_columns, :a_column, :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :table_with_unique_case_insensitive_text_columns
  end
end
