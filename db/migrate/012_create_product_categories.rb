class CreateProductCategories < ActiveRecord::Migration
  def self.up
    create_table :product_categories do |t|
      t.column :name, :string,  :null => false, :limit => 30
    end

    add_index :product_categories,  :name,  :unique => true
  end

  def self.down
    drop_table :product_categories
  end
end
