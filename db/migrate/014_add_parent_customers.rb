class AddParentCustomers < ActiveRecord::Migration
  def self.up
    add_column :customers, :parent_id, :integer, :null => true, :on_delete => :set_null
  end

  def self.down
    remove_column :customers, :parent_id
  end
end
