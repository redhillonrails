class CreateTableWithComment < ActiveRecord::Migration
  def self.up
    create_table :table_with_comment, :comment => "This is a table with a comment" do |t|
    end
  end

  def self.down
    drop_table :table_with_comment
  end
end
