class CreateLogins < ActiveRecord::Migration
  def self.up
    create_table :logins do |t|
      t.column :name,   :string,  :nil => false,  :limit => 255
      t.column :email,  :string,  :nil => false,  :limit => 255
    end

    add_index :logins,  :name,   :unique => true, :case_sensitive => false
    add_index :logins,  :email,  :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :logins
  end
end
