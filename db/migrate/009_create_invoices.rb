class CreateInvoices < ActiveRecord::Migration
  def self.up
    create_table :invoices do |t|
      t.column :order_id,   :integer, :null => false, :on_delete => :cascade
    end

    add_index :invoices, [:order_id], :unique => true
  end

  def self.down
    drop_table :invoices
  end
end
