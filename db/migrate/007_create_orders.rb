class CreateOrders < ActiveRecord::Migration
  def self.up
    create_table :orders do |t|
      t.column :ordered_by_id,      :integer, :null => false, :references => [:customers, :id]
      t.column :customer_reference, :string,  :null => true,  :limit => 30
      t.column :street,             :string,  :null => false, :limit => 30
      t.column :city_id,            :integer, :null => false, :references => :cities
      t.column :phone,              :string,  :null => true,  :limit => 15
      t.column :fax,                :string,  :null => true,  :limit => 15
    end

    add_index :orders, [:ordered_by_id, :customer_reference], :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :orders
  end
end
