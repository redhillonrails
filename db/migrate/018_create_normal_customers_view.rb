class CreateNormalCustomersView < ActiveRecord::Migration
  def self.up
    create_view :normal_customers, "SELECT * FROM customers WHERE LOWER(status) = 'normal'"
  end

  def self.down
    drop_view :normal_customers
  end
end
