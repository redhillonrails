class CreateTableWithLegacyPrimaryKey < ActiveRecord::Migration
  def self.up
    create_table :table_with_legacy_primary_key, :id => false do |t|
      t.primary_key :legacy_id, :references => nil
    end
  end

  def self.down
    drop_table :table_with_legacy_primary_key
  end
end
