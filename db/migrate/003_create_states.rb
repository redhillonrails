class CreateStates < ActiveRecord::Migration
  def self.up
    create_table :states do |t|
      t.column :country_id,   :integer, :null => false
      t.column :name,         :string,  :null => false, :limit => 30
      t.column :abbreviation, :string,  :null => false, :limit => 3
    end

    add_index :states, [:country_id, :name],          :unique => true, :case_sensitive => false
    add_index :states, [:country_id, :abbreviation],  :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :states
  end
end
