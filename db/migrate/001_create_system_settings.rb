class CreateSystemSettings < ActiveRecord::Migration
  def self.up
    create_table :system_settings, :force => true do |t|
      t.column :name,   :string,  :null => false, :limit => 255
      t.column :value,  :text,    :null => false
    end

    add_index :system_settings, [:name], :unique => true, :case_sensitive => false

    SystemSetting.create!(:name => 'environment', :value => RAILS_ENV)
  end

  def self.down
    drop_table :system_settings
  end
end
