class CreateCountries < ActiveRecord::Migration
  def self.up
    create_table :countries, :force => false do |t|
      t.column :name,     :string,  :null => false, :limit => 30
      t.column :isocode,  :string,  :null => false, :limit => 3
      t.column :iddcode,  :string,  :null => false, :limit => 2
    end

    add_index :countries, [:name],    :unique => true, :case_sensitive => false
    add_index :countries, [:isocode], :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :countries
  end
end
