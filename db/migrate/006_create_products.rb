class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.column :name,   :string,  :null => false, :limit => 30
      t.column :price,  :integer, :null => false
      t.column :type,   :string,  :null => false, :limit => 255
    end

    add_index :products, [:type, :name], :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :products
  end
end
