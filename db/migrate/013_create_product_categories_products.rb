class CreateProductCategoriesProducts < ActiveRecord::Migration
  def self.up
    create_table :product_categories_products, :id => false do |t|
      t.column :product_category_id, :integer, :null => false
      t.column :product_id,          :integer, :null => false
    end

    execute("ALTER TABLE product_categories_products ADD PRIMARY KEY (product_category_id, product_id)")
  end

  def self.down
    drop_table :product_categories_products
  end
end
