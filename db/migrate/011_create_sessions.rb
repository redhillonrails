class CreateSessions < ActiveRecord::Migration
  def self.up
    create_table :sessions do |t|
      t.column :session_id, :string,  :references => nil, :limit => 255
      t.column :login_id,   :integer, :nil => false
      t.column :data,       :text
    end

    add_index :sessions,  :session_id,  :unique => true
    add_index :sessions,  :updated_at
  end

  def self.down
    drop_table :sessions
  end
end
