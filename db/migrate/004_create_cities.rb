class CreateCities < ActiveRecord::Migration
  def self.up
    create_table :cities do |t|
      t.column :state_id, :integer, :null => false
      t.column :name,     :string,  :null => false, :limit => 30
      t.column :postcode, :string,  :null => false, :limit => 5
    end

    add_index :cities, [:state_id, :name], :unique => true, :case_sensitive => false
  end

  def self.down
    drop_table :cities
  end
end
