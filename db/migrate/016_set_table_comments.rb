class SetTableComments < ActiveRecord::Migration
  def self.up
    tables.each do |name|
      set_table_comment name, "This table holds #{name.humanize.downcase}"
    end
  end

  def self.down
    tables.each do |name|
      clear_table_comment name
    end
  end
end
